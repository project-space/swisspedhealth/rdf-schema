This repository contains all the information related to the project-specific RDF Schema of the SwissPedHealth, from now on named **SPH Schema** (unlike the SPHN Schema, which corresponds to the re-used concepts of the SPHN).

This repository contains 2 folders:

- **[version-1](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/tree/main/version-1?ref_type=heads)**: First version of the project-specific schema.Aligned with the SPHN schema 2024.2.

- **[version-2](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/tree/main/version-2?ref_type=heads)**: Second version of the project-specific schema. Aligned with the SPHN schema 2025.1.