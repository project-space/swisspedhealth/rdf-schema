All lung function test (LFT) concepts have been created in [v1](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/tree/main/version-1/dataset/LFTs) of the project specific swisspedhealth schema. 
Some of them have changes in the second version v2. 

In this folder you can find the original documents with detailed descriptions. 
In folder[ change_request](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/tree/main/version-2/dataset/LFT_documentation/Change_requests_reviewed_and_approved_in_vs2) you can find the descriptions of the changes that have been made for the second version

The following concepts have been modified under vs 2:
1) Lung Function Measurement Device
2) Lung Function Test Interpretation
3) Bronchodilator Responsiveness
4) Physical Exercise
5) Lung Function Test Quality Control
6) Lung Function Test Session
7) Lung Function Test Result and its child concepts

Minor modifications have been made to the following documents:
[ProcedureSessionMeasurementDevice](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/blob/main/version-2/dataset/LFT_documentation/LFT_ReferenceMetricPanel_ReferenceMetricDetermination_Ethnical_Adjustment_PercentPredicted_Zscore_08052024.docx),
[ReferenceMetricPanel](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/blob/main/version-2/dataset/LFT_documentation/LFT_ReferenceMetricPanel_ReferenceMetricDetermination_Ethnical_Adjustment_PercentPredicted_Zscore_08052024.docx)

Here you can also find the updated [overview ](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/blob/main/version-2/dataset/LFT_documentation/LFT_design_07112024.drawio.pdf)of the lung function test concepts.