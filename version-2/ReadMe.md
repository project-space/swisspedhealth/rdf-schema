**rdf-schema 2025.1**

This repository contains all the information related to version-2 of the project RDF Schema based on SPHN schema vs 2025-1 Prerelease.
Subfolder content:

**dataset**: Excel file of the schema and documents describing the concepts:
         Documentation:
            1) Common concepts
            2) LFT: Lung Function Documentation
            3) Omics Concepts Documentation

**schema**: Turtle (.ttl) file of the Project RDF Schema

**shacl**: SHACL rules. This is to be used only when SHACLs are manually adapted.

**sparql**: SPARQL queries.

**doc**: HTML for the website documentation of the schema.
