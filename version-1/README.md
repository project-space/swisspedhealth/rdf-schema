In this repository we have a set of folders containing the following information:

- **dataset**: Excel file of the project-specific schema (**SPH Schema**) and the documents describing the concepts.

- **schema**: Turtle (.ttl) file of the **SPH Schema**.

- **shacl**: SHACL rules obtained with SchemaForge.

- **sparql**: SPARQL queries obtained with SchemaForge.

- **doc**: HTML for the website documentation of the schema.

The **SPH Schema** incorporates a few new concepts with respect to the SPHN Schema based on the [SPHN Dataset 2024.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/releases/2024-2), but the project-specific concepts inherit some properties from the reused ones.