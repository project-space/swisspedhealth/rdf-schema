This folder contains the documentation of the project-specific concepts of the SwissPedHealth project. These concepts can be split into three parts:

1. Project-specific concepts of common relevance for all the CDWHs (except the Phenotypic Observation, which is only relevant for Kispi Zürich). **THIS PART IS THE ONLY ONE RELEVANT FOR ALL THE CDWHs**. If you are a Data Provider of the SwissPedHealth, please, read the document: [Common_project_specific_concepts_documentation.pdf](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/blob/main/version-1/dataset/Common_project_specific_concepts_documentation.pdf?ref_type=heads).

2. LFTs: Lung Function Test concepts (**ONLY RELEVANT FOR INSELSPITAL AND KISPI ZÜRICH**).

3. OMICS_metadata concepts (**ONLY RELEVANT FOR SMOC**).