**THIS FOLDER IS NOT RELEVANT FOR ALL THE CDWHs. THIS FOLDER IS ONLY RELEVANT FOR SMOC.**

This folder contains the documentation related to the OMICS metadata of the SwissPedHealth project. All the documents are linked on the [DCC DM Atlassian](https://sphn-dcc.atlassian.net/wiki/spaces/NDM/pages/29635108/Concepts+Project+specific) page under the paragraph "other data (omics, eCRF, research data, etc.)". If you cannot access this page, please, send an email to dcc@sib.swiss.

In the future more information related to the OMICS metadata of the SwissPedHealth project may come. For the moment, the reader may refer to the above-mentioned link and to the authors of some of the concepts:

Andrea.Agostini@inf.ethz.ch

Vito.Zanotelli@kispi.uzh.ch