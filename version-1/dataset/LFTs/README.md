**THIS FOLDER IS NOT RELEVANT FOR ALL THE CDWHs. THIS FOLDER IS ONLY RELEVANT FOR INSELSPITAL AND KISPI ZÜRICH.**

This folder contains the documentation of the Lung Function Tests (LFTs), which is a concept of the SwissPedHealth project developed by the team of Philipp Latzin at Inselspital (Anna Hartung and Léa Kim-Mi Ho Dac) with the help of Tatjana Welzel (UKBB) and Harald Witte (DCC). The folder contains the details of the concepts and composedOfs together with a schema in vector format (SVG).

For further questions, contact:

anna.hartung@insel.ch

lea.hodac@insel.ch

The following concepts have been modified under [vs 2](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/tree/main/version-2/dataset/LFT_documentation):
1) Lung Function Measurement Device
2) Lung Function Test Interpretation
3) Bronchodilator Responsiveness
4) Physical Exercise
5) Lung Function Test Quality Control
6) Lung Function Test Session
7) Lung Function Test Result and its child concepts

Please refer to [vs 2](https://git.dcc.sib.swiss/project-space/swisspedhealth/rdf-schema/-/tree/main/version-2/dataset/LFT_documentation) for the most recent documentation.